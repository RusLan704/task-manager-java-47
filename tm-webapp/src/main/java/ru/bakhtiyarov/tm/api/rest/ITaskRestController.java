package ru.bakhtiyarov.tm.api.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.bakhtiyarov.tm.dto.TaskDTO;

import java.util.List;

@RequestMapping(value = "/api/rest/task")
public interface ITaskRestController {
    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO create(
            @RequestBody @Nullable TaskDTO taskDTO
    );

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TaskDTO> findAll();

    @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO findById(
            @PathVariable("id") @Nullable String id
    );

    @PutMapping(value = "/updateById", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO updateTaskById(
            @RequestBody @Nullable TaskDTO taskDTO
    );

    @DeleteMapping(value = "/removeById/{id}")
    void removeOneById(
            @PathVariable("id") @Nullable String id
    );
}
