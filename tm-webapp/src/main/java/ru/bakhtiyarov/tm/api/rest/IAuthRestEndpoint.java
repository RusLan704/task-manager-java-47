package ru.bakhtiyarov.tm.api.rest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.bakhtiyarov.tm.dto.UserDTO;

@RequestMapping("/api/rest/authentication")
public interface IAuthRestEndpoint {
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    boolean login(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    );

    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    UserDTO profile();

    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    void logout();
}
