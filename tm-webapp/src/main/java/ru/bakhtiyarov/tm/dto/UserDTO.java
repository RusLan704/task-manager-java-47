package ru.bakhtiyarov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.enumeration.RoleType;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class UserDTO extends AbstractEntityDTO implements Serializable {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private String middleName = "";

    @Nullable
    private RoleType role = RoleType.USER;

    private Boolean locked = false;

    @NotNull
    public UserDTO(@NotNull String login, @NotNull String passwordHash, @NotNull String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

}