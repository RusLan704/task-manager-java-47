package ru.bakhtiyarov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.bakhtiyarov.tm.enumeration.RoleType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tm_role")
public class Role extends AbstractEntity {

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @Override
    public String toString() {
        return roleType.name();
    }

}
