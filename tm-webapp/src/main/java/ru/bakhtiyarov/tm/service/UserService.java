package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Role;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.RoleType;
import ru.bakhtiyarov.tm.exception.empty.*;
import ru.bakhtiyarov.tm.exception.invalid.InvalidLoginException;
import ru.bakhtiyarov.tm.repository.IUserRepository;

import java.util.Collections;
import java.util.List;

@Service
public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final PasswordEncoder passwordEncoder;

    @NotNull
    @Autowired
    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final String passwordHash = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(RoleType.USER);
        user.setRoles(Collections.singletonList(role));
        return userRepository.save(user);
    }

    @NotNull
    @SneakyThrows
    private User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final String passwordHash = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(RoleType.USER);
        user.setRoles(Collections.singletonList(role));
        return userRepository.save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final String passwordHash = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        return userRepository.save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User updatePassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setPasswordHash(passwordEncoder.encode(password));
        return save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User updateUserEmail(@Nullable final String id, @Nullable final String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setEmail(email);
        return save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User updateUserFirstName(@Nullable final String id, @Nullable final String firstName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setFirstName(firstName);
        return save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User updateUserLastName(@Nullable final String id, @Nullable final String lastName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setLastName(lastName);
        return save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User updateUserMiddleName(@Nullable final String id, @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setMiddleName(middleName);
        return save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User updateUserLogin(@Nullable final String id, @Nullable final String login) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setLogin(login);
        return save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @PreAuthorize("hasRole('ADMIN')")
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull IUserRepository userRepository = getRepository();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = userRepository.findById(id).orElse(null);
        return user;
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    @PreAuthorize("hasRole('ADMIN')")
    public User unLockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    @PreAuthorize("hasRole('ADMIN')")
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    @PreAuthorize("hasRole('ADMIN')")
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    @PreAuthorize("hasRole('ADMIN')")
    public void removeAll() {
        userRepository.deleteAll();
    }

    @NotNull
    @Override
    protected IUserRepository getRepository() {
        return userRepository;
    }

}