package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.dto.CustomUser;
import ru.bakhtiyarov.tm.entity.Role;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.RoleType;
import ru.bakhtiyarov.tm.exception.empty.EmptyLoginException;
import ru.bakhtiyarov.tm.exception.empty.EmptyPasswordException;
import ru.bakhtiyarov.tm.repository.IUserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @Nullable final User user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);

        @NotNull final List<Role> userRoles = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        for (final Role role : userRoles) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .roles(roles.toArray(new String[]{}))
                .password(user.getPasswordHash())
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    public void initUsers() {
        initUsers("test", "test", RoleType.USER);
        initUsers("admin", "admin", RoleType.ADMIN);
    }

    public void initUsers(final String login, String pass, RoleType roleType) {
        final User user = userRepository.findByLogin(login);
        if (user != null) return;
        createUser(login, pass, roleType);
    }

    @Transactional
    public void createUser(@Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final String passwordHash = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

}