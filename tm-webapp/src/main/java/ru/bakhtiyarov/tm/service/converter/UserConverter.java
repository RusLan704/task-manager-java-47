package ru.bakhtiyarov.tm.service.converter;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.api.service.converter.IUserConverter;
import ru.bakhtiyarov.tm.dto.UserDTO;
import ru.bakhtiyarov.tm.entity.User;

@Service
public class UserConverter implements IUserConverter {

    @Nullable
    @Override
    public UserDTO toDTO(@Nullable final User user) {
        if (user == null) return null;
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setMiddleName(user.getMiddleName());
        userDTO.setLocked(user.getLocked());
        return userDTO;
    }

    @Nullable
    @Override
    public User toEntity(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return null;
        User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setMiddleName(userDTO.getMiddleName());
        user.setLocked(userDTO.getLocked());
        return user;
    }
    
}
