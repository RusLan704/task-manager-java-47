package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.Application;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.RoleType;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class ProjectRepositoryTest {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IUserService userService;

    private Project project;

    private User user;

    @Before
    @Transactional
    public void initData() {
        user = userService.create("login", "pass", RoleType.ADMIN);
        project = new Project("name", "description");
        project.setUser(user);
        projectRepository.save(project);
    }

    @After
    @Transactional
    public void deleteData() {
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    @Transactional
    public void testSave() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        @NotNull final Project project = new Project("name2", "description2");
        projectRepository.save(project);
        Assert.assertEquals(2, projectRepository.findAll().size());
        @Nullable final Project projectAdded = projectRepository.getOne(project.getId());
        Assert.assertNotNull(projectAdded);
        Assert.assertEquals(project.getId(), projectAdded.getId());
    }

    @Test
    public void testFindByUserIdAndName() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        Project projectTest = projectRepository.findByUserIdAndName(user.getId(), project.getName());
        Assert.assertNotNull(projectTest);
        Assert.assertEquals(user.getId(), projectTest.getUser().getId());
        Assert.assertEquals(project.getId(), projectTest.getId());
        Assert.assertEquals(project.getName(), projectTest.getName());
        Assert.assertEquals(project.getDescription(), projectTest.getDescription());
    }

    @Test
    public void testFindByUserIdAndId() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        Project projectTest = projectRepository.findByUserIdAndId(user.getId(), project.getId());
        Assert.assertNotNull(projectTest);
        Assert.assertEquals(user.getId(), projectTest.getUser().getId());
        Assert.assertEquals(project.getId(), projectTest.getId());
        Assert.assertEquals(project.getName(), projectTest.getName());
        Assert.assertEquals(project.getDescription(), projectTest.getDescription());
    }

    @Test
    public void testFindAllByUserId() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        @NotNull final Project project = new Project();
        project.setUser(user);
        projectRepository.save(project);
        Assert.assertEquals(2, projectRepository.findAll().size());
        @Nullable final List<Project> tasks = projectRepository.findAllByUserId(project.getUser().getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @Transactional
    public void testDeleteByUserIdAndName() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.deleteByUserIdAndName(project.getUser().getId(),project.getName());
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    @Transactional
    public void testDeleteByUserIdAndId() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.deleteByUserIdAndId(project.getUser().getId(),project.getId());
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    @Transactional
    public void testDeleteAllByUserId() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        @NotNull final Project project = new Project();
        project.setUser(user);
        projectRepository.save(project);
        @Nullable final List<Project> projects = projectRepository.deleteAllByUserId(project.getUser().getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

}