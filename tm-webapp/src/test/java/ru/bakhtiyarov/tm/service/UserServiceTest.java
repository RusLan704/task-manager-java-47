package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.bakhtiyarov.tm.Application;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.RoleType;
import ru.bakhtiyarov.tm.repository.IUserRepository;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class UserServiceTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserRepository userRepository;

    @NotNull
    private PasswordEncoder passwordEncoder;

    private User user1;

    private User user2;

    @Before
    public void initData() {
        user2 = userService.create("login", "pass", RoleType.ADMIN);
        user1 = userService.create("login1", "pass1", RoleType.USER);
        userService.save(user1);
        userService.save(user2);

        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("login", "pass");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void deleteData() {
        userRepository.deleteAll();
    }

    @Test
    public void testCreateLoginPasswordEmail(){
        Assert.assertEquals(2, userService.findAll().size());
        @Nullable final User newUser = userService.create("new login", "password", "email");
        Assert.assertNotNull(newUser);
        Assert.assertEquals(3, userService.findAll().size());
        Assert.assertEquals(newUser.getLogin(), "new login");
        Assert.assertEquals(newUser.getEmail(), "email");
    }

    @Test
    public void testCreateLoginPasswordRole() {
        Assert.assertEquals(2, userService.findAll().size());
        @Nullable final User user = userService.create("new login", "password", RoleType.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(3, userService.findAll().size());
        Assert.assertEquals(user.getLogin(), "new login");
        Assert.assertEquals(user.getRoles().get(0).toString(), RoleType.ADMIN.toString());
    }

    @Test
    public void testUpdatePassword() {
        @Nullable final User nullUser = userService.updatePassword("false userID", "new pass");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updatePassword(user1.getId(), "new pass");
        Assert.assertNotNull(testUser);
        Assert.assertNotEquals(user1.getPasswordHash(), testUser.getPasswordHash());
    }

    @Test
    public void testUpdateUserEmail() {
        @Nullable final User nullUser = userService.updateUserEmail("false userID", "email");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserEmail(user1.getId(), "new email");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getEmail(), "new email");
    }

    @Test
    public void testUpdateUserLogin() {
        @Nullable final User nullUser = userService.updateUserLogin("false userID", "login");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserLogin(user1.getId(), "new login");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getLogin(), "new login");
    }

    @Test
    public void testUpdateFirstName() {
        @Nullable final User nullUser = userService.updateUserFirstName("false userID", "first name");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserFirstName(user1.getId(), "new first name");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getFirstName(), "new first name");
    }

    @Test
    public void testUpdateLastName() {
        @Nullable final User nullUser = userService.updateUserLastName("false userID", "last name");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserLastName(user1.getId(), "new last name");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getLastName(), "new last name");
    }

    @Test
    public void testUpdateMiddleName() {
        @Nullable final User nullUser = userService.updateUserMiddleName("false userID", "mid name");
        Assert.assertNull(nullUser);
        @Nullable final User testUser = userService.updateUserMiddleName(user1.getId(), "new middle name");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getMiddleName(), "new middle name");
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User testUser = userService.findByLogin(user1.getLogin());
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getId(), user1.getId());
        Assert.assertEquals(testUser.getLogin(), user1.getLogin());
    }

    @Test
    public void testLockUserByLogin() {
        Assert.assertEquals(user1.getLocked(), false);
        userService.lockUserByLogin(user1.getLogin());
        final @Nullable User testUser = userService.findByLogin(user1.getLogin());
        Assert.assertEquals(testUser.getId(), user1.getId());
        Assert.assertEquals(testUser.getLocked(), true);
    }

    @Test
    public void testUnLockedUserByLogin() {
        userService.lockUserByLogin(user1.getLogin());
        @Nullable User testUser = userService.findByLogin(user1.getLogin());
        Assert.assertEquals(testUser.getId(), user1.getId());
        Assert.assertEquals(testUser.getLocked(), true);
        userService.unLockUserByLogin(user1.getLogin());
        testUser = userService.findByLogin(user1.getLogin());
        Assert.assertEquals(testUser.getLocked(), false);
    }

    @Test
    public void testRemoveByLogin() {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeByLogin(user1.getLogin());
        Assert.assertEquals(1, userService.findAll().size());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeById(user1.getId());
        Assert.assertEquals(1, userService.findAll().size());
    }

    @Test
    public void testRemoveAll() {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeAll();
        Assert.assertEquals(0, userService.findAll().size());
    }

}