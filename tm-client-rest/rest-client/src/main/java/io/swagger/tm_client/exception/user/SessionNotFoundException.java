package io.swagger.tm_client.exception.user;

import io.swagger.tm_client.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class SessionNotFoundException extends AbstractException {

    @NotNull
    public SessionNotFoundException() {
        super("Error! Session Not found...");
    }

}
