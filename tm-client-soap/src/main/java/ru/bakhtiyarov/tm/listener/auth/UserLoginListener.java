package ru.bakhtiyarov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.endpoint.soap.AuthSoapEndpoint;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.bakhtiyarov.tm.endpoint.soap.TaskSoapEndpoint;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.service.SessionService;
import ru.bakhtiyarov.tm.util.TerminalUtil;

import java.util.List;

@Component
public class UserLoginListener extends AbstractListener {

    @NotNull
    @Autowired
    private AuthSoapEndpoint authEndpoint;

    @NotNull
    @Autowired
    private ProjectSoapEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private TaskSoapEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login user.";
    }

    @EventListener(condition = "@userLoginListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = TerminalUtil.nextLine();
        SessionService.setMaintain(authEndpoint);
        SessionService.setMaintain(projectEndpoint);
        SessionService.setMaintain(taskEndpoint);

        if(!authEndpoint.login(login,password)){
            System.out.println("FAIL");
            return;
        }
        final List<String> session = SessionService.getListCookieRow(authEndpoint);
        SessionService.setListCookieRowRequest(authEndpoint, session);
        SessionService.setListCookieRowRequest(projectEndpoint, session);
        SessionService.setListCookieRowRequest(taskEndpoint, session);
        System.out.println("[OK]");
    }

}
