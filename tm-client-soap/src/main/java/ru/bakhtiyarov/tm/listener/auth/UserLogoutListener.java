package ru.bakhtiyarov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.endpoint.soap.AuthSoapEndpoint;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.bakhtiyarov.tm.endpoint.soap.TaskSoapEndpoint;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.service.SessionService;

import java.util.ArrayList;

@Component
public class UserLogoutListener extends AbstractListener {

    @NotNull
    @Autowired
    private AuthSoapEndpoint authEndpoint;

    @NotNull
    @Autowired
    private ProjectSoapEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private TaskSoapEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout user.";
    }

    @EventListener(condition = "@userLogoutListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        SessionService.setListCookieRowRequest(authEndpoint, new ArrayList<>());
        SessionService.setListCookieRowRequest(projectEndpoint, new ArrayList<>());
        SessionService.setListCookieRowRequest(taskEndpoint, new ArrayList<>());
        System.out.println("[OK]");
    }

}