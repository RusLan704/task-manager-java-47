package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class UnknownSessionException extends AbstractException {

    @NotNull
    public UnknownSessionException() {
        super("Error! Session is unknown...");
    }

}