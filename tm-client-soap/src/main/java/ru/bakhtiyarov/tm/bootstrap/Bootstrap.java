package ru.bakhtiyarov.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.exception.system.UnknownCommandException;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class Bootstrap {

    @Autowired
    private ApplicationEventPublisher publisher;

    public void run(@Nullable final String... args) {
        System.out.println("Welcome to task manager!!!");
        if (parseArgs(args)) System.exit(0);
        @NotNull String command = "";
        while (true) {
            command = TerminalUtil.nextLine();
            try {
                if(command == null || command.isEmpty()) throw new UnknownCommandException();
                publisher.publishEvent(new ConsoleEvent(command));
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    @SneakyThrows
    private boolean parseArgs(@Nullable final String... args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        if (arg == null) return false;
        publisher.publishEvent(new ConsoleEvent(arg));
        return true;
    }

    private static void logError(@NotNull Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

}